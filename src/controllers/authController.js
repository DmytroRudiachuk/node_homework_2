const express = require('express');
const router = express.Router();
const {
    registration,
    signIn
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.post('/register', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    await registration({username, password});

    res.json({
        "message": "Success"
    });
}));

router.post('/login', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const token = await signIn({username, password});

    res.json({"message": "Success", "jwt_token": token});

}));

router.post('/forgot_password', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        username
    } = req.body;
    // TODO: implement forgot password

    res.json({
        "message": "New password sent to your email address"
    });
}));

module.exports = {
    authRouter: router
}