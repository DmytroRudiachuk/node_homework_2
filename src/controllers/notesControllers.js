const express = require('express');
const router = express.Router();

const {
    asyncWrapper
} = require('../utils/apiUtils');

const {
    getUserByUsername
} = require('../services/userService');

const {
    createNewNotes,
    getNotesByUserId,
    deleteUserById
} = require('../services/notesService');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.get('/', asyncWrapper(async (req, res) => {

    const user = await getUserByUsername(req.user.userId)
    const notes = await getNotesByUserId(req.user.userId)

    const offset = req.query.offset
    const limit = req.query.limit

    if (notes === undefined) {
        res.json(notes);
    } else {
        const count = notes.length
        res.json({
            "offset": offset,
            "limit": limit,
            "count": count,
            notes
        });
    }
}));

router.post('/', asyncWrapper(async (req, res) => {

    const text = req.body.text

    const user = await getUserByUsername(req.user.userId)
    const notes = await createNewNotes(req.user.userId, text)

    res.json({
        "message": "Success"
    })
}));

router.get('/:id', (req, res) => {
    res.json({
        "note": {
            "_id": "5099803df3f4948bd2f98391",
            "userId": "5099803df3f4948bd2f98391",
            "completed": false,
            "text": "Complete second homework",
            "createdDate": "2020-10-28T08:03:19.814Z"
        }
    })
})

router.put('/:id', (req, res) => {
    res.json({
        "message": "put success"
    })
})

router.patch('/:id', (req, res) => {
    res.json({
        "message": "patch Success"
    })
})

router.delete('/:id', (req, res) => {
    res.json({
        "message": "Delete seccess"
    })
})


module.exports = {
    notesControllers: router
}