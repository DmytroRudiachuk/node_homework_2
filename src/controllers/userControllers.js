const express = require('express');
const router = express.Router();
const {
    deleteUserById,
    getUserByUsername
} = require('../services/userService');
const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {

    const user = await getUserByUsername(req.user.userId)

    res.json({"user":{
            "_id": user._id,
            "username": user.username,
            "createdDate": user.createdDate
        }
    })
}));

router.delete('/', asyncWrapper(async (req, res) => {

    const user = await deleteUserById(req.user.userId)

    res.json({
        "message": "Deleted successfully"
    });

}));

router.patch('/', asyncWrapper(async (req, res) => {

    // TODO: implement me

    res.json({
        "message": "Password changed successfully"
    });
}));

module.exports = {
    userControllers: router
}