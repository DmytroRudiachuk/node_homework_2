const {User} = require('../models/userModel');

const getUserByUsername = async (userId) => {
    const user = await User.findOne({_id: userId});
    return user;
}

const deleteUserById = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

module.exports = {
    getUserByUsername,
    deleteUserById
};