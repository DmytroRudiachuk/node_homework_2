const {Notes} = require('../models/notesModel');

const createNewNotes = async (userId, text, completed) => {
    const notes = new Notes({
        userId: userId,
        text: text,
        completed: completed
    });
    await notes.save();
}

const getNotesByUserId = async (userId) => {
    const notes = await Notes.find({userId: userId});
    return notes;
}

const deleteUserById = async (userId) => {
    await Notes.findOneAndRemove({userId: userId});
}

module.exports = {
    createNewNotes,
    getNotesByUserId,
    deleteUserById
};