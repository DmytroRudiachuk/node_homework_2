const mongoose = require('mongoose');

const Notes = mongoose.model('Notes', {
    userId: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    text: String,

    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Notes };