const mongoose = require('mongoose');

const User = mongoose.model('UserHW2_new', {
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },

    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { User };
